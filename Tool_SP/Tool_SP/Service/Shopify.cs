﻿using ShopifySharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Tool_SP.Service
{
    public static class Shopify
    {
        public static List<string> CloneStore(string url)
        {
            var storeSitemap = StoreSitemap(url);
            var sitemapProducts = storeSitemap.Where(p => p.Contains("sitemap_products")).ToList();
            if (!sitemapProducts.Any())
                throw new Exception("Product sitemap could not be loaded in current store");

            var productUrls = new List<string>();
            foreach (var sitemapProduct in sitemapProducts)
            {
                var sitemap = ShopifyStorefront.ProductSitemapXml(sitemapProduct);
                //retry to get sitemap
                if (string.IsNullOrEmpty(sitemap))
                    sitemap = ShopifyStorefront.ProductSitemapXml(sitemapProduct);

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(sitemap);
                XmlNodeList locationNodes = xmlDocument.GetElementsByTagName("loc");
                foreach (var node in locationNodes)
                {
                    var xmlNode = node as XmlNode;
                    if (xmlNode == null)
                        continue;
                    if (string.IsNullOrEmpty(xmlNode.InnerText))
                        continue;
                    if (!xmlNode.InnerText.Contains("/products"))
                        continue;
                    productUrls.Add(xmlNode.InnerText);
                }
            }
            return productUrls.Distinct().ToList();

        }
        private static List<string> StoreSitemap(string url)
        {
            var sitemap = ShopifyStorefront.SitemapXml(url);
            //retry to get sitemap
            if (string.IsNullOrEmpty(sitemap))
                sitemap = ShopifyStorefront.SitemapXml(url);
            if (string.IsNullOrEmpty(sitemap))
                throw new Exception("Store site map could not be loaded");
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(sitemap);
            XmlNodeList locationNodes = xmlDocument.GetElementsByTagName("loc");
            var result = new List<string>();
            foreach (var node in locationNodes)
            {
                var xmlNode = node as XmlNode;
                if (xmlNode == null)
                    continue;
                if (string.IsNullOrEmpty(xmlNode.InnerText))
                    continue;
                if (!xmlNode.InnerText.Contains("sitemap_products"))
                    continue;
                result.Add(xmlNode.InnerText);
            }
            return result;
        }
        public static async Task<ShopifySharp.Product> CloneProduct(string originUrl, string adminSpfUrl, string adminSpfPassword)
        {
            //_appUrl = "https://eggdevstore.myshopify.com/";
            //_appApiKey = "da9a782100d300250ab7b245e0daf0ae";
            //_appPassword = "aaf6c8a2beaff8bcfbe6dbb47692e463";
            var productUri = new Uri($"{originUrl}.json");
            var product = await ShopifyStorefront.ExecuteRequestAsync<ShopifySharp.Product>(productUri, HttpMethod.Get, rootElement: "product");
            //retry to get product
            if (product == null)
                product = await ShopifyStorefront.ExecuteRequestAsync<ShopifySharp.Product>(productUri, HttpMethod.Get, rootElement: "product");
            if (product == null)
                throw new Exception($"No product could be loaded with url {originUrl}");
            //Create product with current information
            var _newProduct = new ShopifySharp.Product();
            _newProduct.Title = product.Title;
            _newProduct.BodyHtml = product.BodyHtml;
            _newProduct.ProductType = product.ProductType;
            _newProduct.Tags = product.Tags;
            _newProduct.Vendor = product.Vendor;
            _newProduct.Options = product.Options;
            _newProduct.Metafields = product.Metafields;
            var variants = new List<ProductVariant>();
            foreach (var variant in product.Variants)
            {
                var productVariant = new ProductVariant
                {
                    Price = variant.Price,
                    Title = variant.Title,
                    Option1 = variant.Option1,
                    Option2 = variant.Option2,
                    Option3 = variant.Option3,
                    Position = variant.Position,
                    SKU = variant.SKU,
                };
                variants.Add(productVariant);
            }
            _newProduct.Variants = variants;
            var productService = new ProductService(adminSpfUrl, adminSpfPassword);
            _newProduct = await productService.CreateAsync(_newProduct);

            var _imageService = new ProductImageService(adminSpfUrl, adminSpfPassword);
            //create product image
            foreach (var image in product.Images)
            {
                var productImage = new ProductImage
                {
                    Alt = image.Alt,
                    Attachment = image.Attachment,
                    Src = image.Src,
                    Metafields = image.Metafields
                };
                //select image variant matched
                if (image.VariantIds.Any())
                {
                    var oldVariants = product.Variants.Where(p => image.VariantIds.Contains(p.Id.Value)).ToList();
                    var imageVariantIds = new List<long>();
                    foreach (var oldVariant in oldVariants)
                    {
                        var newVariant = _newProduct.Variants.FirstOrDefault(p => p.Title == oldVariant.Title
                        && p.Option1 == oldVariant.Option1
                        && p.Option2 == oldVariant.Option2
                        && p.Option3 == oldVariant.Option3);
                        if (newVariant == null)
                            continue;
                        imageVariantIds.Add(newVariant.Id.Value);
                    }
                    productImage.VariantIds = imageVariantIds;
                }
                //creat image
                productImage = await _imageService.CreateAsync(_newProduct.Id.Value, productImage);
            }
            await productService.PublishAsync(_newProduct.Id.Value);
            //var productJsUri = new Uri($"{originUrl}.js");
            //var productJs = await ShopifyStorefront.ExecuteRequestAsync(productJsUri, HttpMethod.Get);
            ////retry to get product
            //if (productJs == null)
            //    productJs = await ShopifyStorefront.ExecuteRequestAsync(productJsUri, HttpMethod.Get);
            //if (productJs != null)
            //{
            //    var productObjectJs = productJs.ToObject<ShopifyJs.ProductJs>();
            //    if (productObjectJs != null)
            //    {
            //        var createdImgs = new List<ProductImage>();
            //        foreach (var variant in productObjectJs.Variants)
            //        {
            //            if (variant.FeaturedImage != null)
            //            {
            //                var currentVariant = _newProduct.Variants.FirstOrDefault(p => p.Title == variant.Title && p.Option1 == variant.Option1 && p.SKU == variant.SKU);
            //                if (currentVariant != null)
            //                {
            //                    var variantImage = createdImgs.FirstOrDefault(p => p.Src == variant.FeaturedImage.Src);
            //                    if (variantImage == null)
            //                    {
            //                        variantImage = new ProductImage
            //                        {
            //                            ProductId = _newProduct.Id,
            //                            Position = variant.FeaturedImage.Position,
            //                            Src = variant.FeaturedImage.Src,
            //                            Alt = variant.FeaturedImage.Alt,
            //                            VariantIds = new List<long> { currentVariant.Id.Value}
            //                        };
            //                        variantImage = await _imageService.CreateAsync(_newProduct.Id.Value, variantImage);
            //                        createdImgs.Add(variantImage);
            //                    }
            //                    else
            //                    {
            //                        var ids = variantImage.VariantIds.ToList();
            //                        ids.Add(currentVariant.Id.Value);
            //                        variantImage.VariantIds = ids;
            //                        await _imageService.UpdateAsync(_newProduct.Id.Value, variantImage.Id.Value, variantImage);
            //                    }
            //                }
            //            }
            //        }
            //        //_newProduct = await productService.UpdateAsync(_newProduct.Id.Value, _newProduct);
            //    }

            //}
            return _newProduct;
        }
    }
}
