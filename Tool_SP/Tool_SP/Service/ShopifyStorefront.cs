﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ShopifySharp;
using ShopifySharp.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Tool_SP.Service
{
    public abstract class ShopifyStorefront
    {

        private static IRequestExecutionPolicy _GlobalExecutionPolicy = new DefaultRequestExecutionPolicy();

        private static IRequestExecutionPolicy _ExecutionPolicy = new DefaultRequestExecutionPolicy();

        private static JsonSerializer _Serializer = Serializer.JsonSerializer;

        private static HttpClient _Client { get; } = new HttpClient();

        public static Uri BuildUri(string myShopifyUrl, string path)
        {
            if (Uri.IsWellFormedUriString(myShopifyUrl, UriKind.Absolute) == false)
            {
                if (Uri.IsWellFormedUriString("https://" + myShopifyUrl, UriKind.Absolute) == false)
                {
                    throw new ShopifyException($"The given {nameof(myShopifyUrl)} cannot be converted into a well-formed URI.");
                }

                myShopifyUrl = "https://" + myShopifyUrl;
            }

            var builder = new UriBuilder(myShopifyUrl)
            {
                Scheme = "https:",
                Port = 443, //SSL port
                Path = path
            };
            return builder.Uri;
        }

        public static string BuildRequestUrl(string url, string path)
        {
            if (Uri.IsWellFormedUriString(url, UriKind.Absolute) == false)
            {
                if (Uri.IsWellFormedUriString("https://" + url, UriKind.Absolute) == false)
                {
                    throw new ShopifyException($"The given {nameof(url)} cannot be converted into a well-formed URI.");
                }

                url = "https://" + url;
            }

            var builder = new UriBuilder(url)
            {
                Scheme = "https:",
                Port = 443, //SSL port
                Path = path
            };
            return builder.Uri.ToString();
        }

        protected static CloneableRequestMessage PrepareRequestMessage(RequestUri uri, HttpMethod method, HttpContent content = null)
        {
            var msg = new CloneableRequestMessage(uri.ToUri(), method, content);

            msg.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36");
            msg.Headers.Add("Accept", "application/json");

            return msg;
        }

        public static async Task<JToken> ExecuteRequestAsync(Uri uri, HttpMethod method, HttpContent content = null)
        {
            var requestUri = new RequestUri(uri);
            using (var baseRequestMessage = PrepareRequestMessage(requestUri, method, content))
            {
                var policyResult = await _ExecutionPolicy.Run(baseRequestMessage, async (requestMessage) =>
                {
                    var request = _Client.SendAsync(requestMessage);

                    using (var response = await request)
                    {
                        var rawResult = await response.Content.ReadAsStringAsync();

                        //Check for and throw exception when necessary.
                        CheckResponseExceptions(response, rawResult);

                        JToken jtoken = null;

                        // Don't parse the result when the request was Delete.
                        if (baseRequestMessage.Method != HttpMethod.Delete)
                        {
                            // Make sure that dates are not stripped of any timezone information if tokens are de-serialised into strings/DateTime/DateTimeZoneOffset
                            using (var reader = new JsonTextReader(new StringReader(rawResult)) { DateParseHandling = DateParseHandling.None })
                            {
                                jtoken = JObject.Load(reader);
                            }
                        }

                        return new RequestResult<JToken>(response, jtoken, rawResult);
                    }
                });

                return policyResult;
            }
        }

        public static async Task<T> ExecuteRequestAsync<T>(Uri uri, HttpMethod method, HttpContent content = null, string rootElement = null) where T : new()
        {

            var requestUri = new RequestUri(uri);
            using (var baseRequestMessage = PrepareRequestMessage(requestUri, method, content))
            {
                var policyResult = await _ExecutionPolicy.Run<T>(baseRequestMessage, async (requestMessage) =>
                {
                    var request = _Client.SendAsync(requestMessage);

                    using (var response = await request)
                    {
                        var rawResult = await response.Content.ReadAsStringAsync();

                        //Check for and throw exception when necessary.
                        CheckResponseExceptions(response, rawResult);

                        // This method may fail when the method was Delete, which is intendend.
                        // Delete methods should not be parsing the response JSON and should instead
                        // be using the non-generic ExecuteRequestAsync.
                        var reader = new JsonTextReader(new StringReader(rawResult));
                        var data = _Serializer.Deserialize<JObject>(reader).SelectToken(rootElement);
                        var result = data.ToObject<T>();

                        return new RequestResult<T>(response, result, rawResult);
                    }
                });

                return policyResult;
            }
        }

        public static void CheckResponseExceptions(HttpResponseMessage response, string rawResponse)
        {
            int statusCode = (int)response.StatusCode;

            // No error if response was between 200 and 300.
            if (statusCode >= 200 && statusCode < 300)
            {
                return;
            }

            var requestIdHeader = response.Headers.FirstOrDefault(h => h.Key.Equals("X-Request-Id", StringComparison.OrdinalIgnoreCase));
            string requestId = requestIdHeader.Value?.FirstOrDefault();
            var code = response.StatusCode;

            // If the error was caused by reaching the API rate limit, throw a rate limit exception.
            if ((int)code == 429 /* Too many requests */)
            {
                string listMessage = "Exceeded 2 calls per second for api client. Reduce request rates to resume uninterrupted service.";
                string rateLimitMessage = $"Error: {listMessage}";

                // Shopify used to return JSON for rate limit exceptions, but then made an unannounced change and started returing HTML. 
                // This dictionary is an attempt at preserving what was previously returned.
                var rateLimitErrors = new Dictionary<string, IEnumerable<string>>()
                {
                    {"Error", new List<string>() {listMessage}}
                };

                throw new ShopifyRateLimitException(code, rateLimitErrors, rateLimitMessage, rawResponse, requestId);
            }

            var contentType = response.Content.Headers.GetValues("Content-Type").FirstOrDefault();
            var defaultMessage = $"Response did not indicate success. Status: {(int)code} {response.ReasonPhrase}.";

            if (contentType.StartsWithIgnoreCase("application/json") || contentType.StartsWithIgnoreCase("text/json"))
            {
                var errors = ParseErrorJson(rawResponse);
                string message = defaultMessage;

                if (errors == null)
                {
                    errors = new Dictionary<string, IEnumerable<string>>()
                        {
                            {
                                $"{(int)code} {response.ReasonPhrase}",
                                new string[] { message }
                            },
                        };
                }
                else
                {
                    var firstError = errors.First();

                    message = $"{firstError.Key}: {string.Join(", ", firstError.Value)}";
                }

                throw new ShopifyException(code, errors, message, rawResponse, requestId);
            }
            {
                var errors = new Dictionary<string, IEnumerable<string>>
                {
                    {
                        $"{(int)code} {response.ReasonPhrase}",
                        new string[] { defaultMessage }
                    },
                    {
                        "NoJsonError",
                        new string[] { "Response did not return JSON, unable to parse error message (if any)." }
                    }
                };

                throw new ShopifyException(code, errors, defaultMessage, rawResponse, requestId);
            }
        }

        public static Dictionary<string, IEnumerable<string>> ParseErrorJson(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return null;
            }

            var errors = new Dictionary<string, IEnumerable<string>>();

            try
            {
                var parsed = JToken.Parse(string.IsNullOrEmpty(json) ? "{}" : json);

                // Errors can be any of the following:
                // 1. { errors: "some error message"}
                // 2. { errors: { "order" : "some error message" } }
                // 3. { errors: { "order" : [ "some error message" ] } }
                // 4. { error: "invalid_request", error_description:"The authorization code was not found or was already used" }

                if (parsed.Any(p => p.Path == "error") && parsed.Any(p => p.Path == "error_description"))
                {
                    // Error is type #4
                    var description = parsed["error_description"];

                    errors.Add("invalid_request", new List<string>() { description.Value<string>() });
                }
                else if (parsed.Any(x => x.Path == "errors"))
                {
                    var parsedErrors = parsed["errors"];

                    //errors can be either a single string, or an array of other errors
                    if (parsedErrors.Type == JTokenType.String)
                    {
                        //errors is type #1

                        errors.Add("Error", new List<string>() { parsedErrors.Value<string>() });
                    }
                    else
                    {
                        //errors is type #2 or #3

                        foreach (var val in parsedErrors.Values())
                        {
                            string name = val.Path.Split('.').Last();
                            var list = new List<string>();

                            if (val.Type == JTokenType.String)
                            {
                                list.Add(val.Value<string>());
                            }
                            else if (val.Type == JTokenType.Array)
                            {
                                list = val.Values<string>().ToList();
                            }

                            errors.Add(name, list);
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                errors.Add(e.Message, new List<string>() { json });
            }

            // KVPs are structs and can never be null. Instead, check if the first error equals the default kvp value.
            if (errors.FirstOrDefault().Equals(default(KeyValuePair<string, IEnumerable<string>>)))
            {
                return null;
            }

            return errors;
        }

        public static string SitemapXml(string url)
        {
            var uri = ShopifyStorefront.BuildUri(url, "sitemap.xml");
            var client = new RestClient($"{uri.Scheme}://{uri.Host}");
            var request = new RestRequest(uri, RestSharp.Method.GET);
            request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36");
            var response = client.Execute(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                throw new Exception($"Error occured: {response.ResponseStatus} - {response.Content}");
            return response.Content;
        }
        public static string ProductSitemapXml(string url)
        {
            var uri = new Uri(url);
            var client = new RestClient($"{uri.Scheme}://{uri.Host}");
            var request = new RestRequest(uri, RestSharp.Method.GET);
            request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36");
            var response = client.Execute(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                throw new Exception($"Error occured: {response.ResponseStatus} - {response.Content}");
            return response.Content;
        }
    }
}
