﻿namespace Tool_SP
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCopyShopifyStore = new System.Windows.Forms.Button();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.rtbCrawStoreLogs = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtApiKey = new System.Windows.Forms.TextBox();
            this.txtShopAccessToken = new System.Windows.Forms.TextBox();
            this.txtShopifyUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBaseUrl = new System.Windows.Forms.TextBox();
            this.lblCrawlState = new System.Windows.Forms.Label();
            this.btnPause = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox22.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCopyShopifyStore
            // 
            this.btnCopyShopifyStore.Location = new System.Drawing.Point(635, 385);
            this.btnCopyShopifyStore.Name = "btnCopyShopifyStore";
            this.btnCopyShopifyStore.Size = new System.Drawing.Size(140, 23);
            this.btnCopyShopifyStore.TabIndex = 5;
            this.btnCopyShopifyStore.Text = "Clone Shopify Store";
            this.btnCopyShopifyStore.UseVisualStyleBackColor = true;
            this.btnCopyShopifyStore.Click += new System.EventHandler(this.btnCopyShopifyStore_Click);
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.rtbCrawStoreLogs);
            this.groupBox22.Location = new System.Drawing.Point(12, 120);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(763, 259);
            this.groupBox22.TabIndex = 6;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Crawl Logs";
            // 
            // rtbCrawStoreLogs
            // 
            this.rtbCrawStoreLogs.Location = new System.Drawing.Point(6, 19);
            this.rtbCrawStoreLogs.Name = "rtbCrawStoreLogs";
            this.rtbCrawStoreLogs.Size = new System.Drawing.Size(751, 234);
            this.rtbCrawStoreLogs.TabIndex = 3;
            this.rtbCrawStoreLogs.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(383, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 44;
            this.label6.Text = "Api Key";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(383, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "AccessToken";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(383, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Link Admin Url";
            // 
            // txtApiKey
            // 
            this.txtApiKey.Location = new System.Drawing.Point(464, 83);
            this.txtApiKey.Name = "txtApiKey";
            this.txtApiKey.Size = new System.Drawing.Size(305, 20);
            this.txtApiKey.TabIndex = 41;
            // 
            // txtShopAccessToken
            // 
            this.txtShopAccessToken.Location = new System.Drawing.Point(464, 57);
            this.txtShopAccessToken.Name = "txtShopAccessToken";
            this.txtShopAccessToken.Size = new System.Drawing.Size(305, 20);
            this.txtShopAccessToken.TabIndex = 40;
            // 
            // txtShopifyUrl
            // 
            this.txtShopifyUrl.Location = new System.Drawing.Point(464, 31);
            this.txtShopifyUrl.Name = "txtShopifyUrl";
            this.txtShopifyUrl.Size = new System.Drawing.Size(305, 20);
            this.txtShopifyUrl.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "Base Url";
            // 
            // txtBaseUrl
            // 
            this.txtBaseUrl.Location = new System.Drawing.Point(66, 31);
            this.txtBaseUrl.Margin = new System.Windows.Forms.Padding(2);
            this.txtBaseUrl.Name = "txtBaseUrl";
            this.txtBaseUrl.Size = new System.Drawing.Size(278, 20);
            this.txtBaseUrl.TabIndex = 45;
            // 
            // lblCrawlState
            // 
            this.lblCrawlState.AutoSize = true;
            this.lblCrawlState.Location = new System.Drawing.Point(15, 404);
            this.lblCrawlState.Name = "lblCrawlState";
            this.lblCrawlState.Size = new System.Drawing.Size(0, 13);
            this.lblCrawlState.TabIndex = 48;
            // 
            // btnPause
            // 
            this.btnPause.Location = new System.Drawing.Point(635, 415);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(140, 23);
            this.btnPause.TabIndex = 49;
            this.btnPause.Text = "Pause";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(15, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 50;
            this.label2.Text = "From";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(383, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "To ";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.lblCrawlState);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBaseUrl);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtApiKey);
            this.Controls.Add(this.txtShopAccessToken);
            this.Controls.Add(this.txtShopifyUrl);
            this.Controls.Add(this.groupBox22);
            this.Controls.Add(this.btnCopyShopifyStore);
            this.Name = "Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            this.groupBox22.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCopyShopifyStore;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.RichTextBox rtbCrawStoreLogs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtApiKey;
        private System.Windows.Forms.TextBox txtShopAccessToken;
        private System.Windows.Forms.TextBox txtShopifyUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBaseUrl;
        private System.Windows.Forms.Label lblCrawlState;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

