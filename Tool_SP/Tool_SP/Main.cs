﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tool_SP.Model;
using Tool_SP.Service;

namespace Tool_SP
{
    public partial class Main : Form
    {
        bool runing = false;
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            btnPause.Hide();
        }

        private async void btnCopyShopifyStore_Click(object sender, EventArgs e)
        {
            runing = true;
            btnPause.Show();
            btnCopyShopifyStore.Enabled = false;
            int clonedProducts = 0;
            string adminSpfUrl = txtShopifyUrl.Text;
            string adminSpfPassword = txtShopAccessToken.Text;
            try
            {
                if (!ConfigurationSettings.AppSettings["author"].ToString().Contains("https://www.facebook.com/Boy.IT.05.12.96"))
                    throw new Exception("You do not have permission to use this tool!");
                rtbCrawStoreLogs.Clear();
                lblCrawlState.Text = "Cloning...";
                await Task.Run(() =>
                {
                    List<string> errorLines = new List<string>();
                    try
                    {
                        string _url = txtBaseUrl.Text;
                        if (string.IsNullOrEmpty(_url))
                            throw new Exception("Store shopify url must be required");
                        Invoke(new Action(() =>
                        {
                            lblCrawlState.Text = $"Cloning store {_url} ...";
                            rtbCrawStoreLogs.AppendText(Environment.NewLine);
                            rtbCrawStoreLogs.AppendText($"_____________________{_url}_____________________");
                            rtbCrawStoreLogs.AppendText(Environment.NewLine);
                        }));
                        Uri storeUri = new Uri(_url);
                        string cloneStoreDirectory = $"Clone_Shopify/{storeUri.Host}";

                        if (!Directory.Exists(cloneStoreDirectory))
                        {
                            Directory.CreateDirectory(cloneStoreDirectory);
                        }
                        string _cloneStoreFile = $"{cloneStoreDirectory}/data.csv";
                        List<CloneProduct> _cloneProducts = new List<CloneProduct>();
                        //create clone data if not exist
                        if (!File.Exists(_cloneStoreFile))
                        {
                            var storeProductUrls = Shopify.CloneStore(_url);
                            if (!storeProductUrls.Any())
                                throw new Exception("No store product could be loaded");
                            using (TextWriter writer = File.CreateText(string.Format(_cloneStoreFile)))
                            {
                                var csv = new CsvWriter(writer);
                                csv.Configuration.HasHeaderRecord = true;

                                // print header
                                csv.WriteField("url");
                                csv.WriteField("status");
                                csv.NextRecord();
                                foreach (var url in storeProductUrls)
                                {
                                    //remove query string params to get json data
                                    var uri = new Uri(url);
                                    var importUrl = $"{uri.Scheme}://{uri.Host}{uri.AbsolutePath}";
                                    csv.WriteField(importUrl);
                                    csv.WriteField("pending");
                                    csv.NextRecord();
                                    _cloneProducts.Add(new CloneProduct { Url = importUrl, Status = "pending" });
                                }
                            }

                        }
                        //read clone data and import
                        if (!_cloneProducts.Any())
                        {
                            using (StreamReader sr = new StreamReader(_cloneStoreFile))
                            {
                                string currentLine;

                                while ((currentLine = sr.ReadLine()) != null)
                                {
                                    if (currentLine == "url,status")
                                        continue;
                                    if (!currentLine.Contains(","))
                                        continue;

                                    String[] splits = currentLine.Split(',');
                                    if (splits.Count() == 2)
                                        _cloneProducts.Add(new CloneProduct { Url = splits[0], Status = splits[1] });

                                }
                            }
                        }
                        //on import product to system store
                        string _importStoreFile = $"{cloneStoreDirectory}/import-{DateTime.Now.ToString("MM-dd-yyyy-h-mm-tt")}.csv";
                        int importedCount = 0;
                        using (TextWriter writer = File.CreateText(string.Format(_importStoreFile)))
                        {
                            var csv = new CsvWriter(writer);
                            csv.Configuration.HasHeaderRecord = true;

                            // print header
                            csv.WriteField("url");
                            csv.WriteField("state");
                            csv.WriteField("message");
                            csv.NextRecord();
                            int indexLinkImport = 0;
                            while (indexLinkImport < _cloneProducts.Count)
                            {
                                if (!runing)
                                    break;
                                var cloneProduct = _cloneProducts[indexLinkImport];
                                indexLinkImport++;
                                if (cloneProduct == null)
                                    continue;
                                if (cloneProduct.Status == "imported")
                                    continue;
                                Invoke(new Action(() =>
                                {
                                    lblCrawlState.Text = $"Cloning product {cloneProduct.Url} ...";
                                }));
                                try
                                {
                                    Shopify.CloneProduct(cloneProduct.Url, adminSpfUrl, adminSpfPassword).Wait();
                                    //update record
                                    csv.WriteField(cloneProduct.Url);
                                    csv.WriteField("success");
                                    cloneProduct.Status = "imported";
                                    Invoke(new Action(() =>
                                    {
                                        rtbCrawStoreLogs.AppendText(Environment.NewLine);
                                        rtbCrawStoreLogs.AppendText($"Imported product - {cloneProduct.Url}");
                                    }));
                                    importedCount++;
                                }
                                catch (Exception ex)
                                {
                                    csv.WriteField(cloneProduct.Url);
                                    csv.WriteField("error");
                                    csv.WriteField(ex.Message);
                                    cloneProduct.Status = "failed";
                                    Invoke(new Action(() =>
                                    {
                                        rtbCrawStoreLogs.AppendText(Environment.NewLine);
                                        rtbCrawStoreLogs.AppendText($"Error import product - {cloneProduct.Url} - {ex.Message}");
                                    }));
                                }
                                finally
                                {
                                    csv.NextRecord();
                                }
                            }
                        }
                        if (importedCount > 0)
                        {
                            Invoke(new Action(() =>
                            {
                                rtbCrawStoreLogs.AppendText(Environment.NewLine);
                                rtbCrawStoreLogs.AppendText($"Imported: {importedCount} product(s)");
                                rtbCrawStoreLogs.AppendText(Environment.NewLine);
                            }));
                        }
                        clonedProducts += importedCount;
                        //update import data
                        using (TextWriter writer = File.CreateText(string.Format(_cloneStoreFile)))
                        {
                            var csv = new CsvWriter(writer);
                            csv.Configuration.HasHeaderRecord = true;

                            // print header
                            csv.WriteField("url");
                            csv.WriteField("status");
                            csv.NextRecord();
                            foreach (var cloneResult in _cloneProducts)
                            {
                                //remove query string params to get json data
                                csv.WriteField(cloneResult.Url);
                                csv.WriteField(cloneResult.Status);
                                csv.NextRecord();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Invoke(new Action(() =>
                        {
                            rtbCrawStoreLogs.AppendText($"Clone store failed: {ex.Message}");
                            rtbCrawStoreLogs.AppendText(Environment.NewLine);
                            errorLines.Add(ex.Message);
                        }));
                    }
                    if (errorLines.Any())
                    {
                        Invoke(new Action(() =>
                        {
                            rtbCrawStoreLogs.AppendText(Environment.NewLine);
                            rtbCrawStoreLogs.AppendText("*************************ERROR************************");
                            rtbCrawStoreLogs.AppendText(Environment.NewLine);
                            foreach (var line in errorLines)
                            {
                                rtbCrawStoreLogs.AppendText(line);
                                rtbCrawStoreLogs.AppendText(Environment.NewLine);
                            }
                            rtbCrawStoreLogs.AppendText("************************************************************");

                        }));
                    }
                });
            }
            catch (Exception ex)
            {
                lblCrawlState.Text = "Clone shopify store failed";
                MessageBox.Show($"An error occured: {ex.Message}", "Coppied shopify store error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btnPause.Hide();
                lblCrawlState.Text = $"Cloned {clonedProducts} products to system shopify store";
                btnCopyShopifyStore.Enabled = true;
                runing = false;
            }
            
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            runing = false;
            lblCrawlState.Text = "Please wait a moment! Stopping import.......";
        }
    }
    
}
